package address.test;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import address.domain.Address;
import address.repo.AddressDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/test-context.xml"
,"classpath:/META-INF/spring/applicationContext.xml"
})
@Transactional
@TransactionConfiguration(defaultRollback=true)
public class AddressDaoTest {

	@Autowired
    private AddressDao addressDao;
	
	@Test
    public void testFindById()
    {
		Address address = addressDao.findById(1);
		
		Assert.assertEquals("Jose de Alencar", address.getRua());
		Assert.assertEquals("20", address.getNumero());
		Assert.assertEquals("20A", address.getComplemento());
		Assert.assertEquals("Vila Marchi", address.getBairro());
		Assert.assertEquals("S�o Bernardo do Campo", address.getCidade());
		Assert.assertEquals("S�o Paulo", address.getEstado());
		Assert.assertEquals("09810450", address.getCep());
    }
	
	@Test
	public void testCreate() {
		Address address = new Address();
		address.setCep("09810450");
		address.setRua("Jose de Alencar");
		address.setNumero(20);
		address.setBairro("Vila Marchi");
		address.setCidade("S�o Bernardo do Campo");
		address.setEstado("S�o Paulo");
		address.setComplemento("20A");
		
		addressDao.save(address);
		Integer id = address.getId();
		Assert.assertNotNull(id);
		
		Address newAddress = addressDao.findById(id);
		
		Assert.assertEquals("Jose de Alencar", newAddress.getRua());
		Assert.assertEquals("09810450", newAddress.getCep());
		Assert.assertEquals("Vila Marchi", newAddress.getBairro());
		Assert.assertEquals("S�o Bernardo do Campo", newAddress.getCidade());
		Assert.assertEquals("S�o Paulo", newAddress.getEstado());
		Assert.assertEquals("20", newAddress.getNumero());
		Assert.assertEquals("20A", newAddress.getComplemento());
	}
	
	@Test
	public void testUpdate() {
		Address address = addressDao.findById(1);
		
		address.setComplemento("Assun��o");
		
		addressDao.update(address);
		Integer id = address.getId();
		Assert.assertNotNull(id);
		
		Address newAddress = addressDao.findById(id);
		
		Assert.assertEquals("Jose de Alencar", newAddress.getRua());
		Assert.assertEquals("09810450", newAddress.getCep());
		Assert.assertEquals("Vila Marchi", newAddress.getBairro());
		Assert.assertEquals("S�o Bernardo do Campo", newAddress.getCidade());
		Assert.assertEquals("S�o Paulo", newAddress.getEstado());
		Assert.assertEquals("20", newAddress.getNumero());
		Assert.assertEquals("Assun��o", newAddress.getComplemento());
	}
	
	@Test
	public void testDelete() {
		Address address = addressDao.findById(1);
		addressDao.update(address);
		
		Address newAddress = addressDao.findById(1);
		Assert.assertNull(newAddress);
	}

}
