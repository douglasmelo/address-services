package address.repo;

import address.domain.Address;

public interface AddressDao {

	public Address findById(Integer id);
	
	public Address save(Address address);
	
	public void update(Address address);
	
	public void remove(Address address);
}
