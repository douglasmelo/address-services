package address.mvc;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import address.domain.Address;
import address.service.AddressBo;
import address.to.AddressResultObjectDeleteTO;
import address.to.AddressResultObjectErrorTO;
import address.to.AddressResultObjectTO;
import address.to.AddressResultObjectViaCepTO;

/**
 * Class that represent the services of address, has the method search by zipcode and the CRUD services for the address.
 * @author douglas
 *
 */
@Controller
@RequestMapping("/controller/address")
public class AddressServiceController {
	
	protected static final String PRODUCES_JSON = MediaType.APPLICATION_JSON + ";charset=UTF-8";
	
	@Autowired
	private AddressBo addressBo;
	
	/**
	 * Find address by id.
	 * @param id
	 * @return
	 */
	@RequestMapping(value ="/{id}", method = RequestMethod.GET, produces = PRODUCES_JSON)
	@ResponseBody
	public ResponseEntity<Object> findById(@PathVariable("id") Integer id){
		try {
			Address result = addressBo.findById(id);
			return new ResponseEntity<Object>(result, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<Object>(new AddressResultObjectErrorTO("Erro: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Create new address with payload information and validade the zipcode, the parameters rua, bairro, cidade and estado could change with the zipcode was not correct.
	 * @param payload
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = PRODUCES_JSON, produces = PRODUCES_JSON)
	@ResponseBody
	public ResponseEntity<Object> create(@RequestBody Address payload){
		try {
			String zipcode = getNextValidZipcode(payload.getCep(),null);
			payload.setCep(zipcode);
			ResponseEntity<Object> addressResult = getAddress(zipcode);
			if(addressResult.getStatusCode().value() == 200) {
				AddressResultObjectTO addressResultObjectTO = (AddressResultObjectTO) addressResult.getBody();
				payload.setRua(addressResultObjectTO.getRua());
				payload.setBairro(addressResultObjectTO.getBairro());
				payload.setCidade(addressResultObjectTO.getCidade());
				payload.setEstado(addressResultObjectTO.getEstado());
			}
			else {
				return new ResponseEntity<Object>(new AddressResultObjectErrorTO("CEP inv�lido"), HttpStatus.valueOf(addressResult.getStatusCode().value()));
			}
			Address address = addressBo.save(payload);
			return new ResponseEntity<Object>(address, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<Object>(new AddressResultObjectErrorTO("Erro: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Update new address with payload information and validade the zipcode, the parameters rua, bairro, cidade and estado could change with the zipcode was not correct.
	 * @param payload
	 * @return
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.PUT, consumes = PRODUCES_JSON, produces = PRODUCES_JSON)
	@ResponseBody
	public ResponseEntity<Object> update(@RequestBody Address payload){
		try {
			String zipcode = getNextValidZipcode(payload.getCep(),null);
			payload.setCep(zipcode);
			ResponseEntity<Object> addressResult = getAddress(zipcode);
			if(addressResult.getStatusCode().value() == 200) {
				AddressResultObjectTO addressResultObjectTO = (AddressResultObjectTO) addressResult.getBody();
				payload.setRua(addressResultObjectTO.getRua());
				payload.setBairro(addressResultObjectTO.getBairro());
				payload.setCidade(addressResultObjectTO.getCidade());
				payload.setEstado(addressResultObjectTO.getEstado());
			}
			else {
				return new ResponseEntity<Object>(new AddressResultObjectErrorTO("CEP inv�lido"), HttpStatus.valueOf(addressResult.getStatusCode().value()));
			}
			addressBo.update(payload);
			return new ResponseEntity<Object>(payload, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<Object>(new AddressResultObjectErrorTO("Erro: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Delete an address by id.
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/{id}", method = RequestMethod.DELETE, produces = PRODUCES_JSON)
	@ResponseBody
	public ResponseEntity<Object> delete(@PathVariable("id") Integer id){
		try {
			Address address = addressBo.findById(id);
			addressBo.remove(address);
			return new ResponseEntity<Object>(new AddressResultObjectDeleteTO("Ok"), HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<Object>(new AddressResultObjectErrorTO("Erro: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Service that received the zipcode and return the address correspondent
	 * @param zipcode
	 * @return
	 */
	@RequestMapping(value="/zipcode", method=RequestMethod.GET, produces="application/json")
	public ResponseEntity<Object> getAddress(@QueryParam("zipcode") String zipcode) {
		AddressResultObjectTO result = null;
		AddressResultObjectViaCepTO tempResult = null;
		zipcode = getNextValidZipcode(zipcode,null);
		ClientRequest cr = new ClientRequest("http://viacep.com.br/ws/" + zipcode + "/json/");
		cr.accept("application/json");
		cr.header("Content-Type:", "charset=UTF-8");
		try {
			ClientResponse<AddressResultObjectViaCepTO> clientResponse = cr.get(AddressResultObjectViaCepTO.class);
			if (clientResponse.getStatus() == 200) {
				tempResult = clientResponse.getEntity();
				if(tempResult.getErro() == null) {
					result = new AddressResultObjectTO(tempResult.getLogradouro(), tempResult.getBairro(), tempResult.getLocalidade(), tempResult.getUf());
				}
			} else {
				return new ResponseEntity<Object>(new AddressResultObjectErrorTO("CEP inv�lido"), HttpStatus.valueOf(clientResponse.getStatus()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(new AddressResultObjectErrorTO("CEP inv�lido"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}
	
	/**
	 * Method that return the next valid zipcode, if the zipcode does not return the address, the rightest digit was substituted by 0 until return an address. 
	 * @param zipcode
	 * @param time
	 * @return
	 */
	public String getNextValidZipcode(String zipcode, Integer time){
		String result = "";
		AddressResultObjectViaCepTO tempResult = null;
		ClientResponse<AddressResultObjectViaCepTO> clientResponse = null;
		ClientRequest cr = new ClientRequest("http://viacep.com.br/ws/" + zipcode + "/json/");
		cr.accept("application/json");
		cr.header("Content-Type:", "charset=UTF-8");
		
		try {
			clientResponse = cr.get(AddressResultObjectViaCepTO.class);
			if (clientResponse.getStatus() == 200) {
				tempResult = clientResponse.getEntity();
				if(tempResult.getErro() != null) {
					if(time == null) {
						time = 0;
					}
					if(time == 0) {
						zipcode = zipcode.substring(0, zipcode.length()-1) + "0";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 1) {
						zipcode = zipcode.substring(0, zipcode.length()-2) + "00";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 2) {
						zipcode = zipcode.substring(0, zipcode.length()-3) + "000";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 3) {
						zipcode = zipcode.substring(0, zipcode.length()-4) + "0000";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 4) {
						zipcode = zipcode.substring(0, zipcode.length()-5) + "00000";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 5) {
						zipcode = zipcode.substring(0, zipcode.length()-6) + "000000";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 6) {
						zipcode = zipcode.substring(0, zipcode.length()-7) + "0000000";
						result = getNextValidZipcode(zipcode, ++time);
					}else if(time == 7) {
						zipcode = zipcode.substring(0, zipcode.length()-8) + "00000000";
					}
				}else {
					result = zipcode;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
