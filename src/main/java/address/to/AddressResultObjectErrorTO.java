package address.to;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressResultObjectErrorTO implements Serializable{

	private static final long serialVersionUID = 2005414617279276094L;
	
	public String erro;
	
	public AddressResultObjectErrorTO(String erro) {
		super();
		this.erro = erro;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}
	
}
