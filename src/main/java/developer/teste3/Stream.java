package developer.teste3;

/**
 * Interface that represents the Stream behavior.
 * @author dmelo
 *
 */
public interface Stream {

    public char getNext();
    
    public boolean hasNext();
    
}
