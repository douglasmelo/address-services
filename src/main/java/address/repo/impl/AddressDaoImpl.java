package address.repo.impl;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import address.domain.Address;
import address.repo.AddressDao;

@Repository
@Transactional
public class AddressDaoImpl implements AddressDao{
	
	@Autowired
    private EntityManager em;

	@Override
	public Address findById(Integer id) {
		return em.find(Address.class, id);
	}

	@Override
	public Address save(Address address) {
		em.persist(address);
		em.flush();
		return address;
	}

	@Override
	public void update(Address address) {
		em.merge(address);
		em.flush();
	}

	@Override
	public void remove(Address address) {
		Address result = em.find(Address.class, address.getId());
		em.remove(result);
	}

}
