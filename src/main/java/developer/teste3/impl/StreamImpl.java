package developer.teste3.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import developer.teste3.Stream;

/**
 * Class that implements the Stream interface
 * @author dmelo
 *
 */
public class StreamImpl implements Stream{
    
    private String sequence;
    private int currentPosition;
    
    /**
     * Constructor of Class StreamImpl that receive the sequence as parameter and initialized the currentPossition with Zero.
     * @param sequence
     */
    public StreamImpl(String sequence) {
        this.sequence = sequence;
        this.currentPosition = 0;
    }

    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        Stream input = new StreamImpl("aAbbBABacc");
        
        char firstChar = firstChar(input);
        if(firstChar == ' ') {
            System.out.println("Não foi possível encontrar o primeiro caracter que não se repita no resto da Stream");
        }else {
            System.out.println(firstChar);
        }
    }
    
    /**
     * Method that received Steam input as parameter and return the first char not repeated on the stream, otherwise return a empty char ' '.
     * @param input
     * @return
     */
    public static char firstChar(Stream input) {
        Map<Character, Integer> mapFrequency = new TreeMap<Character, Integer>();
        Character result = null;
        while(input.hasNext()) {
            char next = input.getNext();
            
            if(mapFrequency.containsKey(next)) {
                Integer frenquency = mapFrequency.get(next);
                mapFrequency.put(next, ++frenquency);
                
            }else {
                mapFrequency.put(next,1);
            }
        }
        
        Iterator<Character> iterator = mapFrequency.keySet().iterator();
        while(iterator.hasNext()) {
            Character key = iterator.next();
            if(mapFrequency.get(key) == 1) {
                result = key;
                break;
            }
        }
        
        if(result == null) {
            return ' ';
        }else {
            return result.charValue();
        }
    }

    /**
     * Implemented method of Stream interface that return the next char on the sequence.
     */
    @Override
    public char getNext() {
        char nextChar = sequence.charAt(currentPosition);
        currentPosition++;
        return nextChar;
    }

    /**
     * Implemented method of Stream interface that check if has more char on the sequence. Return true if has and false otherwise.
     */
    @Override
    public boolean hasNext() {
        if(currentPosition < sequence.length()) {
            return true;
        }
        return false;
    }
}
