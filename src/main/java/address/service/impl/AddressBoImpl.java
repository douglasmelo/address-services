package address.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import address.domain.Address;
import address.repo.AddressDao;
import address.service.AddressBo;

@Component("addressBo")
public class AddressBoImpl implements AddressBo{
	
	@Autowired
	private AddressDao addressDao;

	@Override
	public Address findById(Integer id) {
		Address address = addressDao.findById(id);
		return address;
	}

	@Override
	public Address save(Address address) {
		return addressDao.save(address);
	}

	@Override
	public void update(Address address) {
		addressDao.update(address);
	}

	@Override
	public void remove(Address address) {
		addressDao.remove(address);
		
	}

}
