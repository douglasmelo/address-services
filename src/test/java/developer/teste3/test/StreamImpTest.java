package developer.teste3.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import developer.teste3.Stream;
import developer.teste3.impl.StreamImpl;

public class StreamImpTest {

    @Test
    public void shouldReturnbAsFirstSequence() {
        Stream input = new StreamImpl("aAbBABac");
        char firstChar = StreamImpl.firstChar(input);
        assertTrue(firstChar == 'b');
    }
    
    @Test
    public void shouldReturncAsFirstSequence() {
        Stream input = new StreamImpl("aAbbBABac");
        char firstChar = StreamImpl.firstChar(input);
        assertTrue(firstChar == 'c');
    }
    
    @Test
    public void shouldReturnEmptyCharAsFirstSequence() {
        Stream input = new StreamImpl("aAbbBABacc");
        char firstChar = StreamImpl.firstChar(input);
        assertTrue(firstChar == ' ');
    }
    
    @Test
    public void shouldReturnEAsFirstSequence() {
        Stream input = new StreamImpl("aAbbBAEBacc");
        char firstChar = StreamImpl.firstChar(input);
        assertTrue(firstChar == 'E');
    }
    
    @Test
    public void shouldReturnEmptyAsFirstSequenceOnMoreThanThreeCharacter() {
        Stream input = new StreamImpl("aAbbBAEEEBacc");
        char firstChar = StreamImpl.firstChar(input);
        assertTrue(firstChar == ' ');
    }

}
