package address.to;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressResultObjectDeleteTO implements Serializable{

	private static final long serialVersionUID = -6837354340045976148L;

	private String status;
	
	public AddressResultObjectDeleteTO(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
